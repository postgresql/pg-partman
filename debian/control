Source: pg-partman
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders:
 Christoph Berg <myon@debian.org>,
 Adrian Vondendriesch <adrian.vondendriesch@credativ.de>,
Build-Depends:
 architecture-is-64-bit <!pkg.postgresql.32-bit>,
 debhelper-compat (= 13),
 libtap-parser-sourcehandler-pgtap-perl,
 postgresql-17-pgtap,
 postgresql-all <!nocheck>,
 postgresql-server-dev-all,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/postgresql/pg-partman
Vcs-Git: https://salsa.debian.org/postgresql/pg-partman.git
Homepage: https://github.com/pgpartman/pg_partman

Package: postgresql-17-partman
Architecture: any
Depends:
 python3,
 python3-psycopg2,
 ${misc:Depends},
 ${postgresql:Depends},
 ${shlibs:Depends},
Description: PostgreSQL Partition Manager
 pg_partman is a PostgreSQL extension to create and manage both time-based and
 serial-based table partition sets. Sub-partitioning is also supported. Child
 table & trigger function creation is all managed by the extension itself.
 Tables with existing data can also have their data partitioned in easily
 managed smaller batches. Optional retention policy can automatically drop
 partitions no longer needed. A background worker (BGW) process is included to
 automatically run partition maintenance without the need of an external
 scheduler (cron, etc) in most cases.
